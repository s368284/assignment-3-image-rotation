#include "rotate.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "image.h"

void __rotate(const struct image *input, struct image *output, int angle)
{
    uint64_t width = input->width;
    uint64_t height = input->height;

    switch (angle)
    {
    case 0:
        for (uint64_t i = 0; i < width * height; ++i)
        {
            output->data[i] = input->data[i];
        }
        break;
    case 90:
        for (int y = 0; y < height; ++y)
        {
            for (int x = 0; x < width; ++x)
            {
                output->data[(width - 1 - x) * height + y] = input->data[y * width + x];
            }
        }
        break;
    case 180:
        for (int y = 0; y < height; ++y)
        {
            for (int x = 0; x < width; ++x)
            {
                output->data[(height - 1 - y) * width + (width - 1 - x)] = input->data[y * width + x];
            }
        }
        break;
    case 270:
        for (int y = 0; y < height; ++y)
        {
            for (int x = 0; x < width; ++x)
            {
                output->data[x * height + (height - 1 - y)] = input->data[y * width + x];
            }
        }
        break;
    }
}

enum transform_status rotate(const struct image *input, struct image *output, int angle)
{
    if (angle % 90 != 0)
    {
        // Поддерживаются только углы, кратные 90 градусам
        return TRANSFORM_ERROR;
    }

    angle = (angle + 360) % 360;

    if (angle % 180 != 0)
    {
        output->width = input->height;
        output->height = input->width;
    }
    else
    {
        output->width = input->width;
        output->height = input->height;
    }

    output->data = malloc(output->width * output->height * sizeof(struct pixel));
    if (output->data == NULL)
    {
        return TRANSFORM_ERROR;
    }

    __rotate(input, output, angle);

    return TRANSFORM_OK;
}
