#pragma once

#include <stdio.h>
#include <stdlib.h>

#include "image.h"

enum transform_status
{
    TRANSFORM_OK = 0,
    TRANSFORM_ERROR
};

enum transform_status rotate(const struct image *input, struct image *output, int angle);
