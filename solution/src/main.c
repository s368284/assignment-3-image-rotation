#include <stdio.h>
#include <stdlib.h>

#include "bmp_io.h"
#include "image.h"
#include "rotate.h"

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

int main(int argc, char **argv)
{

    if (argc != 4)
    {
        fprintf(stderr, "Использовать следующим образом %s <source-image> <transformed-image> <angle>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const char *source_path = argv[1];
    const char *dest_path = argv[2];
    int angle = atoi(argv[3]);

    struct image img;
    enum read_status read_result = from_bmp_file(source_path, &img);

    if (read_result != READ_OK)
    {
        fprintf(stderr, "Ошибка при чтении изображения: %d\n", read_result);
        return EXIT_FAILURE;
    }

    struct image ans;
    enum transform_status rotate_res = rotate(&img, &ans, angle);
    if (rotate_res != TRANSFORM_OK)
    {
        fprintf(stderr, "Ошибка при повороте изображения %d\n", read_result);
        return EXIT_FAILURE;
    }
    free(img.data);

    enum write_status write_res = to_bmp_file(dest_path, &ans);

    if (write_res != WRITE_OK)
    {
        fprintf(stderr, "Ошибка при записи изображения: %d\n", write_res);
        return EXIT_FAILURE;
    }

    printf("Изображение успешно прочитано\n");
    
    free(ans.data);

    return EXIT_SUCCESS;
}
