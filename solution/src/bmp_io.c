#include "bmp_io.h"

#include <stdio.h>
#include <stdlib.h>

#include "image.h"

const int BMP_PADDING_SIZE = 4;
const int BMP_TYPE = 0x4D42;
const int HEADER_SIZE = 40;

static size_t get_padding(size_t width)
{
    return (BMP_PADDING_SIZE - (width * sizeof(struct pixel)) % BMP_PADDING_SIZE) % BMP_PADDING_SIZE;
}

void fillBmpHeader(struct bmp_header *header, uint32_t width, uint32_t height, uint32_t padding)
{
    uint32_t size_image = (width * (sizeof(struct pixel)) + padding) * height;

    header->bfType = BMP_TYPE;
    header->bfileSize = size_image + sizeof(struct bmp_header);
    header->bfReserved = 0;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = HEADER_SIZE;
    header->biWidth = width;
    header->biHeight = height;
    header->biPlanes = 1;
    header->biBitCount = sizeof(struct pixel) * 8;
    header->biCompression = 0;
    header->biSizeImage = size_image;
    header->biXPelsPerMeter = 1;
    header->biYPelsPerMeter = 1;
    header->biClrUsed = 0;
    header->biClrImportant = 0;
}

enum read_status from_bmp_file(const char *filename, struct image *img)
{
    FILE *file = fopen(filename, "rb");

    if (file == NULL)
    {
        perror("error opening file");
        return READ_ERROR;
    }

    struct bmp_header header;

    fread(&header, sizeof(struct bmp_header), 1, file);

    if (header.bfType != 0x4D42)
    {
        fprintf(stderr, "not a bmp file\n");
        fclose(file);
        return READ_INVALID_SIGNATURE;
    }

    fseek(file, header.bOffBits, SEEK_SET);

    img->data = (struct pixel *)malloc(header.biSizeImage);
    if (img->data == NULL)
    {
        return READ_MALLOC_ERROR;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;

    const uint64_t padding = get_padding(header.biWidth);

    for (uint32_t i = 0; i < header.biHeight; i++)
    {
        size_t num = fread((img->data) + i * header.biWidth, sizeof(struct pixel), header.biWidth, file);
        if (num == 0)
        {
            return READ_ERROR;
        }
        fseek(file, (long)padding, SEEK_CUR);
    }

    fclose(file);

    return READ_OK;
}

enum write_status to_bmp_file(const char *filename, const struct image *img)
{
    FILE *file = fopen(filename, "wb");

    if (!file)
    {
        perror("Error opening file");
        return WRITE_ERROR;
    }

    uint32_t padding_size = get_padding(img->width);

    struct bmp_header header;
    fillBmpHeader(&header, img->width, img->height, padding_size);

    if (fwrite(&header, sizeof(struct bmp_header), 1, file) != 1)
    {
        return WRITE_HEADER_ERROR;
    }

    for (uint32_t y = 0; y < img->height; y++)
    {
        fwrite(&img->data[img->width * y], sizeof(struct pixel), img->width, file);
        fseek(file, padding_size, SEEK_CUR);
    }

    fclose(file);

    return WRITE_OK;
}
